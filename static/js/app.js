/**
 * appName - http://chidi-frontend.esy.es/
 * @version v0.1.0
 * @author bev-olga@yandex.ru
 */
(function() {


}).call(this);



(function(e){function t(){var e=document.createElement("input"),t="onpaste";return e.setAttribute(t,""),"function"==typeof e[t]?"paste":"input"}var n,a=t()+".mask",r=navigator.userAgent,i=/iphone/i.test(r),o=/android/i.test(r);e.mask={definitions:{9:"[0-9]",a:"[A-Za-z]","*":"[A-Za-z0-9]"},dataName:"rawMaskFn",placeholder:"_"},e.fn.extend({caret:function(e,t){var n;if(0!==this.length&&!this.is(":hidden"))return"number"==typeof e?(t="number"==typeof t?t:e,this.each(function(){this.setSelectionRange?this.setSelectionRange(e,t):this.createTextRange&&(n=this.createTextRange(),n.collapse(!0),n.moveEnd("character",t),n.moveStart("character",e),n.select())})):(this[0].setSelectionRange?(e=this[0].selectionStart,t=this[0].selectionEnd):document.selection&&document.selection.createRange&&(n=document.selection.createRange(),e=0-n.duplicate().moveStart("character",-1e5),t=e+n.text.length),{begin:e,end:t})},unmask:function(){return this.trigger("unmask")},mask:function(t,r){var c,l,s,u,f,h;return!t&&this.length>0?(c=e(this[0]),c.data(e.mask.dataName)()):(r=e.extend({placeholder:e.mask.placeholder,completed:null},r),l=e.mask.definitions,s=[],u=h=t.length,f=null,e.each(t.split(""),function(e,t){"?"==t?(h--,u=e):l[t]?(s.push(RegExp(l[t])),null===f&&(f=s.length-1)):s.push(null)}),this.trigger("unmask").each(function(){function c(e){for(;h>++e&&!s[e];);return e}function d(e){for(;--e>=0&&!s[e];);return e}function m(e,t){var n,a;if(!(0>e)){for(n=e,a=c(t);h>n;n++)if(s[n]){if(!(h>a&&s[n].test(R[a])))break;R[n]=R[a],R[a]=r.placeholder,a=c(a)}b(),x.caret(Math.max(f,e))}}function p(e){var t,n,a,i;for(t=e,n=r.placeholder;h>t;t++)if(s[t]){if(a=c(t),i=R[t],R[t]=n,!(h>a&&s[a].test(i)))break;n=i}}function g(e){var t,n,a,r=e.which;8===r||46===r||i&&127===r?(t=x.caret(),n=t.begin,a=t.end,0===a-n&&(n=46!==r?d(n):a=c(n-1),a=46===r?c(a):a),k(n,a),m(n,a-1),e.preventDefault()):27==r&&(x.val(S),x.caret(0,y()),e.preventDefault())}function v(t){var n,a,i,l=t.which,u=x.caret();t.ctrlKey||t.altKey||t.metaKey||32>l||l&&(0!==u.end-u.begin&&(k(u.begin,u.end),m(u.begin,u.end-1)),n=c(u.begin-1),h>n&&(a=String.fromCharCode(l),s[n].test(a)&&(p(n),R[n]=a,b(),i=c(n),o?setTimeout(e.proxy(e.fn.caret,x,i),0):x.caret(i),r.completed&&i>=h&&r.completed.call(x))),t.preventDefault())}function k(e,t){var n;for(n=e;t>n&&h>n;n++)s[n]&&(R[n]=r.placeholder)}function b(){x.val(R.join(""))}function y(e){var t,n,a=x.val(),i=-1;for(t=0,pos=0;h>t;t++)if(s[t]){for(R[t]=r.placeholder;pos++<a.length;)if(n=a.charAt(pos-1),s[t].test(n)){R[t]=n,i=t;break}if(pos>a.length)break}else R[t]===a.charAt(pos)&&t!==u&&(pos++,i=t);return e?b():u>i+1?(x.val(""),k(0,h)):(b(),x.val(x.val().substring(0,i+1))),u?t:f}var x=e(this),R=e.map(t.split(""),function(e){return"?"!=e?l[e]?r.placeholder:e:void 0}),S=x.val();x.data(e.mask.dataName,function(){return e.map(R,function(e,t){return s[t]&&e!=r.placeholder?e:null}).join("")}),x.attr("readonly")||x.one("unmask",function(){x.unbind(".mask").removeData(e.mask.dataName)}).bind("focus.mask",function(){clearTimeout(n);var e;S=x.val(),e=y(),n=setTimeout(function(){b(),e==t.length?x.caret(0,e):x.caret(e)},10)}).bind("blur.mask",function(){y(),x.val()!=S&&x.change()}).bind("keydown.mask",g).bind("keypress.mask",v).bind(a,function(){setTimeout(function(){var e=y(!0);x.caret(e),r.completed&&e==x.val().length&&r.completed.call(x)},0)}),y()}))}})})(jQuery);

    var active_housing;
    var active_floor;
    var active_flat;
    var need_load;
    var flat_href;
    var need_url;

$(document).ready(function(){

    var current_zoom = 1;
    var timeout_link;

    $('.fancy').fancybox();

    // формат цифр
    function number_format( str ){
        return str.replace(/(\s)+/g, '').replace(/(\d{1,3})(?=(?:\d{3})+$)/g, '$1 ');
    };

    if($('.flats-app-container').length){


        var s = Snap("#svg"),
            attr = {
                fill: '#76CCF4',
                opacity: '0'
            },
            attr_hover = {
                fill: '#76CCF4',
                opacity: '0.5'
            },
            i = 0,
            arr = new Array(),
            href_floor;

            Snap.load("../../../img/genplan.svg", function (f) {
                var g = f.select("g");
                s.append(g);

                 for (var i in plan) {
                    var obj = s.path(plan[i].path);
                    var available = plan[i].available;
                    var url = plan[i].url;

                    obj.attr(attr).attr({'class': 'floor_' +plan[i].floor});
                    arr[obj.id] = i;

                    obj.hover(function(){
                        this.attr(attr_hover);
                    },function(){
                        this.attr(attr);
                        }
                    )
                    if (available == 'true'){
                        obj.click(function(){
                            active_floor = plan[arr[this.id]].floor;
                            show_floor(plan[arr[this.id]].href, plan[arr[this.id]].url, plan[arr[this.id]].floor, plan[arr[this.id]].housing);
                            $('.flat').addClass('open');
                        })
                    }



                    arr[obj.id] = i;

                    i++;
                }


            });


        $(document).on('click', '.js--open-filer', function(){
            $('.flats-filter-wrap').toggleClass('active')
        });

        function filter_init(){
            $('.slider-component').each(function(){
                var parent = $(this).parents('.field-block');
                var min_range = Number($(this).attr('data-min_range'));
                var max_range = Number($(this).attr('data-max_range'));
                var min_current = Number($(this).attr('data-min-current'));
                var max_current = Number($(this).attr('data-max-current'));
                var step = Number($(this).attr('data-step'));
                var arr = [];

                arr.push(min_current);
                arr.push(max_current);
                $(this).slider({
                    range: true,
                    min: min_range,
                    max: max_range,
                    values: arr,
                    step: step,
                    start: function(event, ui){
                        if(parent.parents('.filter-section').hasClass('size-section')){
                            parent.find('.square_min').css('left', parent.find('.slider-component .ui-slider-handle:nth-child(2)').css('left'));
                            parent.find('.square_max').css('left', parent.find('.slider-component .ui-slider-handle:nth-child(3)').css('left'));
                        }
                    },
                    change: function (event, ui) {
                        var min_current = Number(ui.values[0]);
                        var max_current = Number(ui.values[1]);
                        var min_current_int = min_current;
                        var max_current_int = max_current;
                        min_current = min_current.toString();
                        min_current = number_format(min_current);
                        max_current = max_current.toString();
                        max_current = number_format(max_current);
                        if(parent.parents('.filter-section').hasClass('.price-section')) {
                            parent.find('.inp-price-min').prop('value', min_current_int );
                            parent.find('.price_value.price_min').text(min_current + " руб.");
                            parent.find('.inp-price-max').prop('value', max_current_int );
                            parent.find('price_value.price_max').text(max_current + " руб.");
                        }
                        if(parent.parents('.filter-section').hasClass('size-section')){
                            setTimeout(function(){
                                parent.find('.square_min').css('left', parent.find('.slider-component .ui-slider-handle:nth-child(2)').css('left'));
                                parent.find('.square_max').css('left', parent.find('.slider-component .ui-slider-handle:nth-child(3)').css('left'));

                                parent.find('.inp-square-min').prop('value', min_current);
                                parent.find('.square_min').text(min_current + " м");
                                parent.find('.inp-square-max').prop('value', max_current);
                                parent.find('.square_max').text(max_current + " м");
                            },10)
                        }
                        if(parent.parents('.filter-section').hasClass('location-section')){
                            setTimeout(function(){
                                parent.find('.square_min').css('left', parent.find('.slider-component .ui-slider-handle:nth-child(2)').css('left'));
                                parent.find('.square_max').css('left', parent.find('.slider-component .ui-slider-handle:nth-child(3)').css('left'));

                                parent.find('.inp-square-min').prop('value', min_current);
                                parent.find('.square_min').text(min_current);
                                parent.find('.inp-square-max').prop('value', max_current);
                                parent.find('.square_max').text(max_current);
                            },10)
                        }
                        if (timeout_link) {
                            clearTimeout(timeout_link)
                        }

                        timeout_link = setTimeout(function () {
                            send_filter();
                        }, 250);
                    },
                    slide: function (event, ui) {
                        var min_current = Number(ui.values[0]);
                        var max_current = Number(ui.values[1]);
                        var min_current_int = min_current;
                        var max_current_int = max_current;
                        min_current = min_current.toString();
                        min_current = number_format(min_current);
                        max_current = max_current.toString();
                        max_current = number_format(max_current);
                        if(parent.parents('.filter-section').hasClass('.price-section')) {
                            parent.find('.inp-price-min').prop('value', min_current_int );
                            parent.find('.price_value.price_min').text(min_current + " руб.");
                            parent.find('.inp-price-max').prop('value', max_current_int );
                            parent.find('price_value.price_max').text(max_current + " руб.");
                        }
                        if(parent.parents('.filter-section').hasClass('size-section')){
                            setTimeout(function(){
                                parent.find('.square_min').css('left', parent.find('.slider-component .ui-slider-handle:nth-child(2)').css('left'));
                                parent.find('.square_max').css('left', parent.find('.slider-component .ui-slider-handle:nth-child(3)').css('left'));

                                parent.find('.inp-square-min').prop('value', min_current);
                                parent.find('.square_min').text(min_current + " м");
                                parent.find('.inp-square-max').prop('value', max_current);
                                parent.find('.square_max').text(max_current + " м");
                            },10)
                        }
                        if(parent.parents('.filter-section').hasClass('location-section')){
                            setTimeout(function(){
                                parent.find('.square_min').css('left', parent.find('.slider-component .ui-slider-handle:nth-child(2)').css('left'));
                                parent.find('.square_max').css('left', parent.find('.slider-component .ui-slider-handle:nth-child(3)').css('left'));

                                parent.find('.inp-square-min').prop('value', min_current);
                                parent.find('.square_min').text(min_current);
                                parent.find('.inp-square-max').prop('value', max_current);
                                parent.find('.square_max').text(max_current);
                            },10)
                        }
                        if (timeout_link) {
                            clearTimeout(timeout_link)
                        }

                        timeout_link = setTimeout(function () {
                            send_filter();
                        }, 250);
                    }
                });
            });
        }

        filter_init();

        // вертикальный скролл
        setTimeout(function(){
            $('.ps-container').perfectScrollbar();
        },10);

        // отправка фильтра
        $(document).on('click', '.js--send-filter', function(){
            send_filter();
        });

        function send_filter(){
            var form = $('.filter-form');
            var method = form.attr('method');
            var url = form.attr('action');

            $.ajax({
                url: url,
                method: method,
                data: form.serialize(),
                success: function (data) {
                    var parse_data = jQuery.parseJSON(data);
                    $('.results-rows').empty().append(parse_data.content);
                }
            });
        }

        // резет фильтра
        $(document).on('click', '.js--filter-reset', function(){
            $('.flats-filter-container label .hidden-block').attr('checked', false);
            filter_init();
            send_filter();
        });

        // закрытие этажа
        $(document).on('click', '.js--close-floor', function(){
            $('.flat').removeClass('open');

            var url = window.location.href;
            if($('.flats-navigator-screen-flat .floor-nav').length){
                var url = window.location.href;
                url = url.slice(0,-1);
                var leng = url.length -1;

                if(url[leng] != '/') {
                    url = url.slice(0,-2);
                }
                else{
                    url = url.slice(0,-1);
                }
            }
            url = url.slice(0,-6);

            $('.flats-navigator-screen-flat').addClass('hidden-block');
            $('.flats-navigator-screen-floor').addClass('hidden-block');
            $('.flats-navigator-screen-flat .floor-screen-content').empty();
            $('.flats-navigator-screen-floor .floor-screen-content').empty();

            var stateParameters = { foo: "bar" };
            history.pushState(stateParameters, "New page title", url);

            need_url = true;
        });


        // закрытие квартиры
        $(document).on('click', '.js--close-flat', function(){

            var url = window.location.href;
            url = url.slice(0,-1);
            var leng = url.length -1;

            if(url[leng] != '/') {
                url = url.slice(0,-2);
            }
            else{
                url = url.slice(0,-1);
            }

            $('.flats-navigator-screen-flat').addClass('hidden-block');
            $('.flats-navigator-screen-flat .floor-screen-content').empty();

            var stateParameters = { foo: "bar" };
            history.pushState(stateParameters, "New page title", url);
        });


        // этаж вверх
        $(document).on('click', '.floor-nav-up', function(){
            var floor = Number($('.floor-nav-pos span:nth-child(1)').text());

            if(floor != 3){
                $(".svg--floor").empty();
                $('.floor-nav-pos span:nth-child(1)').text(floor + 1);
                active_floor = Number(active_floor) + 1;

                var attr_st = {fill: '#35aab7'},
                    attr_1 = {fill: '#ffdd00'},
                    attr_2 = {fill: '#8fb852'},
                    attr_3 = {fill: '#f47b20'},
                    i = 0,
                    arr_floor = new Array()
                    arr_load = "housing_" + active_housing + "_floor_" + active_floor,
                    floor_container = Snap(".svg--floor");


                for (var i in floor_data[arr_load]) {
                    var obj = floor_container.path(floor_data[arr_load][i].path);
                    var type = floor_data[arr_load][i].type;
                    var available = floor_data[arr_load][i].available;

                    if (type == 'st') {obj.attr(attr_st).attr({'class': 'flat_' + floor_data[arr_load][i].flat});}
                    if (type == '1') {obj.attr(attr_1).attr({'class': 'flat_' + floor_data[arr_load][i].flat});}
                    if (type == '2') {obj.attr(attr_2).attr({'class': 'flat_' + floor_data[arr_load][i].flat});}
                    if (type == '3') {obj.attr(attr_3).attr({'class': 'flat_' + floor_data[arr_load][i].flat});}

                    arr_floor[obj.id] = i;
                    i++;

                    if (available == 'true'){
                        obj.click(function () {
                            active_flat = arr_load[arr_floor[this.id]].flat;
                            show_flat(arr_load[arr_floor[this.id]].href, active_flat);
                        })
                    }
                    else{
                        obj.attr({'class': 'noav'});
                    }
                }

                if(active_floor == 1 && active_housing == 2){
                     Snap.load("../../../img/1-2-1.svg", function (f) {
                        var g = f.select("g");
                        floor_container.append(g);
                    });

                }
                if((active_floor == 2 || active_floor == 3) && active_housing == 2){
                    Snap.load("../../../img/1-2-t.svg", function (f) {
                        g = f.select("g");
                        floor_container.append(g);
                    });
                }

                if(active_floor == 1 && active_housing == 1){
                     Snap.load("../../../img/1-1-1.svg", function (f) {
                        var g = f.select("g");
                        floor_container.append(g);
                    });

                }
                if(active_floor == 2 && active_housing == 1){
                    Snap.load("../../../img/1-1-2.svg", function (f) {
                        g = f.select("g");
                        floor_container.append(g);
                    });
                }
                if(active_floor == 3 && active_housing == 1){
                    Snap.load("../../../img/1-1-2.svg", function (f) {
                        g = f.select("g");
                        floor_container.append(g);
                    });
                }

                var url = window.location.href;
                url = url.slice(0,-1) + (floor + 1);
                floor_number(floor + 1)
                var stateParameters = { foo: "bar" };
                history.pushState(stateParameters, "New page title", url);
            }
            else{
                return false;
            }

        })

        // этаж вниз
        $(document).on('click', '.floor-nav-down', function(){
            var floor = Number($('.floor-nav-pos span:nth-child(1)').text());

            if(floor != 1){
                $(".svg--floor").empty();
                $('.floor-nav-pos span:nth-child(1)').text(floor - 1);
                active_floor = Number(active_floor) - 1;

                var attr_st = {fill: '#35aab7'},
                    attr_1 = {fill: '#ffdd00'},
                    attr_2 = {fill: '#8fb852'},
                    attr_3 = {fill: '#f47b20'},
                    i = 0,
                    arr_floor = new Array()
                    arr_load = "housing_" + active_housing + "_floor_" + active_floor,
                    floor_container = Snap(".svg--floor");


                for (var i in floor_data[arr_load]) {
                    var obj = floor_container.path(floor_data[arr_load][i].path);
                    var type = floor_data[arr_load][i].type;
                    var available = floor_data[arr_load][i].available;

                    if (type == 'st') {obj.attr(attr_st).attr({'class': 'flat_' + floor_data[arr_load][i].flat});}
                    if (type == '1') {obj.attr(attr_1).attr({'class': 'flat_' + floor_data[arr_load][i].flat});}
                    if (type == '2') {obj.attr(attr_2).attr({'class': 'flat_' + floor_data[arr_load][i].flat});}
                    if (type == '3') {obj.attr(attr_3).attr({'class': 'flat_' + floor_data[arr_load][i].flat});}

                    arr_floor[obj.id] = i;
                    i++;

                    if (available == 'true'){
                        obj.click(function () {
                            active_flat = arr_load[arr_floor[this.id]].flat;
                            show_flat(arr_load[arr_floor[this.id]].href, active_flat);
                        })
                    }
                    else{
                        obj.attr({'class': 'noav'});
                    }
                }

                if(active_floor == 1 && active_housing == 2){
                     Snap.load("../../../img/1-2-1.svg", function (f) {
                        var g = f.select("g");
                        floor_container.append(g);
                    });

                }
                if((active_floor == 2 || active_floor == 3) && active_housing == 2){
                    Snap.load("../../../img/1-2-t.svg", function (f) {
                        g = f.select("g");
                        floor_container.append(g);
                    });
                }

                if(active_floor == 1 && active_housing == 1){
                     Snap.load("../../../img/1-1-1.svg", function (f) {
                        var g = f.select("g");
                        floor_container.append(g);
                    });

                }
                if(active_floor == 2 && active_housing == 1){
                    Snap.load("../../../img/1-1-2.svg", function (f) {
                        g = f.select("g");
                        floor_container.append(g);
                    });
                }
                if(active_floor == 3 && active_housing == 1){
                    Snap.load("../../../img/1-1-2.svg", function (f) {
                        g = f.select("g");
                        floor_container.append(g);
                    });
                }

                var url = window.location.href;
                url = url.slice(0,-1) + (floor - 1);
                floor_number(floor - 1)
                var stateParameters = { foo: "bar" };
                history.pushState(stateParameters, "New page title", url);
            }
            else{
                return false;
            }


        });

        // показать попап расчетов
        $(document).on('click', '.js--show-popup', function(){
            $(this).addClass('open')
        });
        $(document).on('click', '.js--close-popup', function(){
            $(this).parents('.calc-wrap').removeClass('open')
            return false;
        });


        // показать квартиру из фильтра
        $(document).on('click', '.js--show-flat', function(){
            need_load = true;
            active_flat = $(this).attr('data-flat');
            flat_href = $(this).attr('data-flat-href');
            show_floor($(this).attr('data-href'), $(this).attr('data-link'), $(this).attr('data-floor'), $(this).attr('data-housing'));
            return false;
        })
    }

    // фоторама
    if($('.fotorama').length){
        $('.fotorama')
        .on('fotorama:show fotorama:showend',
          function (e, fotorama, extra) {
            var index = Number(fotorama.activeIndex) + 1;
            $('.about-inner--text').text('Фото ' + index + " из " + fotorama.size)
          }
        )
        .fotorama();
    }

    // перетаскивалка карты
    if($('.about-location').length){
        $( ".map-close" ).draggable({
        });

        $( ".map-far" ).draggable({
        });
    };

    // зум и анзум карты
    $(document).on('click', '.plus', function(){
        if(current_zoom == 4){
            return false;
        }
        if(current_zoom == 3){
            current_zoom = 4;
            $('.map-close').animate({
              width: "+=604",
              height: "+=434",
              left: "-=302",
              top: "-=217"
            }, 0);
            return false;
        }
        if(current_zoom == 2){
            current_zoom = 3;
            $(".map-far").addClass('hidden-block');
            $(".map-close").removeClass('hidden-block')
            return false;
        }
        if(current_zoom == 1){
            current_zoom = 2;
            $('.map-far').animate({
              width: "+=614",
              height: "+=455",
              left: "-=307",
              top: "-=227"
            }, 0);
        }
    });

    $(document).on('click', '.minus', function(){
        if(current_zoom == 1){
            return false;
        }
        if(current_zoom == 4) {
            current_zoom = 3;
            $('.map-close').animate({
                 width: "-=604",
                 height: "-=434",
                 left: "+=302",
                 top: "+=217"
            }, 0);
            return false;
        }
        if(current_zoom == 3){
             current_zoom = 2;
             $(".map-far").removeClass('hidden-block');
             $(".map-close").addClass('hidden-block')
             return false;
        }
        if(current_zoom == 2){
             current_zoom = 1;
             $('.map-far').animate({
             width: "-=614",
             height: "-=455",
             left: "+=307",
             top: "+=227"
             }, 100);
        }
    })

    // схема района
    if($('.contacts .map').length){
        var office = $(this).attr('data-office');
        var complecs = $(this).attr('data-complecs');
        var myMap;

        ymaps.ready(function () {
        var myMap = new ymaps.Map('map-contacts', {
                center: [60.089302,30.382538],
                zoom: 10
            }, {}),
            myPlacemark_ofc = new ymaps.Placemark([59.957219,30.342687], {}, {
                iconLayout: 'default#image',
                iconImageHref: '../../../img/icon--map-ofc.png',
                // Размеры метки.
                iconImageSize: [78, 99],
                // Смещение левого верхнего угла иконки относительно
                // её "ножки" (точки привязки).
                iconImageOffset: [-39, -119]
            }),
            myPlacemark_comp = new ymaps.Placemark([60.177407,30.365285], {}, {
                iconLayout: 'default#image',
                iconImageHref: '../../../img/icon--map-zhk.png',
                // Размеры метки.
                iconImageSize: [78, 99],
                // Смещение левого верхнего угла иконки относительно
                // её "ножки" (точки привязки).
                iconImageOffset: [-39, -119]
            });

            myMap.geoObjects.add(myPlacemark_ofc);
            myMap.geoObjects.add(myPlacemark_comp);
            myMap.behaviors.disable('scrollZoom');
            myMap.behaviors.disable('drag');
        });
    }

    if($('.structure .map').length){

        function init(){
            var map = new ymaps.Map("map-structure", {
                center: [60.158363,30.387237],
                zoom: 12
                }),
                myPoint = new ymaps.Placemark([55.76, 37.64], {}, {
                    iconLayout: 'default#image',
                    iconImageHref: '../../../img/icon--map.png',
                    iconImageSize: [62, 76],
                    iconImageOffset: [-39, -119]
                });

                xs_inject(map,presets);

        }
        ymaps.ready(init);

        function xs_inject(map,presets) {

            var collections = {}
            var icons = {}
                var wrapper = ymaps.templateLayoutFactory.createClass(
                    '$[[options.contentLayout observeSize minWidth=235 maxWidth=235 maxHeight=350]]'
                );
                var balloon = ymaps.templateLayoutFactory.createClass(
                    '<div class="map_balloon" style="background-color:{{ properties.bgcolor}};color:{{ properties.color }}">{{ properties.name }}</div>'
                );

                for (var pi in presets) {
                    collections[pi] = new ymaps.GeoObjectCollection({});
                    for (var i = 0; i < presets[pi].length; i++) {
                        var data = presets[pi][i]
                        collections[pi].add(new ymaps.Placemark([data[0], data[1]],
                            {
                                name: data[5],
                                color: data[4],
                                bgcolor: data[3],
                                hintContent: null
                            }, {
                                iconLayout: 'default#image',
                                iconImageHref: data[2],
                                iconImageSize: [29, 29],
                                balloonLayout: wrapper,
                                balloonContentLayout: balloon,
                                balloonPanelMaxMapArea: 0,
                                balloonPanelMaxMapArea: 0,
                                hideIconOnBalloonOpen: false,
                                balloonOffset: [3, -10]
                            }));
                    }
                }

                map.show_preset = function (index) {
                    for (var pi in collections) {
                        map.geoObjects.remove(collections[pi])
                    }
                    map.geoObjects.add(collections[index]);
                }

                map.show_all = function () {
                    for (var pi in collections) {
                        map.geoObjects.add(collections[pi])
                    }
                }

                map.show_all()


                $('.presets .preset').click(function () {
                    $('.presets li').removeClass('active');
                    $(this).addClass('active');
                    var $this = $(this)
                    map.show_preset($this.data('preset'))
                    return false;
                })

                $('.presets .all').click(function () {
                    map.show_all()
                    return false;
                })
        }
    }

    $(document).on('click', '.popup-gallery .close', function(){
        $('.popup-gallery').removeClass('active').find('.gallery-container').empty();
    })


    $(document).on('click', '.js--load-fotorama', function(){
        var url = $(this).attr('href');
        startindex = $(this).attr('data-startindex');
            $.ajax({
                url: url,
                method: "post",
                success: function (data) {
                    $('.popup-gallery').addClass('active').find('.gallery-container').append(data);
                    $('.gallery-container .fotorama').fotorama();
                }
            });
        return false;
    })

    // фоторама
    if($('.about-photo .fotorama').length){
        $('.about-photo .fotorama')
        .on('fotorama:show fotorama:showend',
          function (e, fotorama, extra) {
            var index = Number(fotorama.activeIndex) + 1;
            $('.about-photo--text .number').text('Фото ' + index + " из " + fotorama.size)
          }
        )
        .fotorama();
    }

    $(document).on('click', '.timeline-box__el', function(){
        if(!$(this).hasClass('active') && !$(this).hasClass('disable')){
            var index = $(this).index();
            $('.timeline-triangle').animate({
              left: (15 + Number(index) * 180)
            }, 200);
            $('.timeline-box__el').removeClass('active');
            $('.timeline-box__el').removeClass('color');
            $(this).addClass('active');
            $('.timeline-line').css('width', Number(index) * 182)
            for (var i = 0; i <= index; i++) {
                $('.timeline-box__el').eq(i).addClass('color')
            }

            $('.about-photo .month').text($(this).find('span').attr('data-text'))

             var url = $(this).attr('data-url');

            $.ajax({
                url: url,
                method: "post",
                success: function (data) {
                    $('.fotorama-wrap').empty().append(data)
                    $('.fotorama-wrap .fotorama').on('fotorama:show fotorama:showend',
                      function (e, fotorama, extra) {
                        var index = Number(fotorama.activeIndex) + 1;
                        $('.about-photo--text .number').text('Фото ' + index + " из " + fotorama.size)
                      }
                    ).fotorama();
                }
            });
        }
    })

    //  отправка формы
    $(document).on('click', '.call-request .send', function () {

        var form =  $(this).parents('.form-content');
        var errors = false;

        $(form).find('.is-required').each(function(){
            var val = $(this).find('input').prop('value');
            if(val=='' || val==undefined){
                $(this).find('.errors').text('Неверно заполнено поле');
                errors=true;
            }
        });

        if(errors == false){
            var button_value = $(form).find('.js-form-submit').prop('value');
            $(form).find('.js-form-submit').prop('value','Подождите...');

            var method = form.attr('method');
            var action = form.attr('action');
            var data = form.serialize();
            $.ajax({
                type: method,
                url: action,
                data: data,
                success: function(data) {
                    $('.js--send-ok').trigger('click');
                },
                error: function(data) {
                    $(form).find('.js-form-submit').prop('value', 'Ошибка');
                    setTimeout(function() {
                        $(form).find('.js-form-submit').prop('value', button_value);
                    }, 2000);
                }
            });
        }

        return false;
    });

})

    // количество квартир на этаж
    function floor_number(floor){

        var link = $('.flats-navigator-screen-floor .floor-nav').attr('data-url')
        $.ajax({
            url: link,
            method: 'post',
            data: {'floor' : floor},
            success: function (data) {
                var parse_data = jQuery.parseJSON(data);
                $('.flats-navigator-screen-floor .plan-circles .circle-medium:last-child .heading').text(parse_data.number)
            }
        });
    }

    function show_floor(link, url, floor, housing){
    // вывов попапа с этажами

        $('.flats-navigator-screen-floor').removeClass('hidden-block');
        active_housing = housing;

        $.ajax({
            url: link,
            method: 'post',
            success: function (data) {
                var parse_data = jQuery.parseJSON(data);

                $('.flats-navigator-screen-floor .floor-screen-content').append(parse_data.content);

                var floor_container = Snap(".svg--floor");
                $('.floor-nav-pos span:nth-child(1)').text(active_floor);

                floor_number(active_floor);

                var attr_st = {fill: '#35aab7'},
                    attr_1 = {fill: '#ffdd00'},
                    attr_2 = {fill: '#8fb852'},
                    attr_3 = {fill: '#f47b20'},
                    i = 0,
                    arr_floor = new Array()
                    arr_load = "housing_" + housing + "_floor_" + floor;


                for (var i in floor_data[arr_load]) {
                    var obj = floor_container.path(floor_data[arr_load][i].path);
                    var type = floor_data[arr_load][i].type;
                    var available = floor_data[arr_load][i].available;

                    if (type == 'st') {obj.attr(attr_st).attr({'class': 'flat_' + floor_data[arr_load][i].flat});}
                    if (type == '1') {obj.attr(attr_1).attr({'class': 'flat_' + floor_data[arr_load][i].flat});}
                    if (type == '2') {obj.attr(attr_2).attr({'class': 'flat_' + floor_data[arr_load][i].flat});}
                    if (type == '3') {obj.attr(attr_3).attr({'class': 'flat_' + floor_data[arr_load][i].flat});}

                    arr_floor[obj.id] = i;
                    i++;

                    if (available == 'true'){
                        obj.click(function () {
                            active_flat = arr_load[arr_floor[this.id]].flat;
                            show_flat(arr_load[arr_floor[this.id]].href, active_flat);
                        })
                    }
                    else{
                        obj.attr({'class': 'noav'});
                    }
                }

                if(active_floor == 1 && housing == 2){
                     Snap.load("../../../img/1-2-1.svg", function (f) {
                        var g = f.select("g");
                        floor_container.append(g);
                    });

                }
                if((active_floor == 2 || active_floor == 3) && housing == 2){
                    Snap.load("../../../img/1-2-t.svg", function (f) {
                        g = f.select("g");
                        floor_container.append(g);
                    });
                }

                if(active_floor == 1 && housing == 1){
                     Snap.load("../../../img/1-1-1.svg", function (f) {
                        var g = f.select("g");
                        floor_container.append(g);
                    });

                }
                if(active_floor == 2 && housing == 1){
                    Snap.load("../../../img/1-1-2.svg", function (f) {
                        g = f.select("g");
                        floor_container.append(g);
                    });
                }
                if(active_floor == 3 && housing == 1){
                    Snap.load("../../../img/1-1-2.svg", function (f) {
                        g = f.select("g");
                        floor_container.append(g);
                    });
                }

                if(need_load == true){
                    show_flat(flat_href, active_flat);
                    need_load = false;
                    flat_href = '';
                    active_flat = '';
                }
            }
        });

        if(need_url != false) {
             var stateParameters = {foo: "bar"};
             history.pushState(stateParameters, "New page title", window.location.href + url);
        }
    }

    // вывов попапа с квартирой
    function show_flat(link, flat){

        $('.flats-navigator-screen-flat').removeClass('hidden-block');

        $.ajax({
            url: link,
            method: 'post',
            success: function (data) {
                $('.flats-navigator-screen-flat .floor-screen-content').append(data);



                $('.calc-panel .slider-component').each(function(){
                    var min_range = Number($(this).attr('data-min_range'));
                    var max_range = Number($(this).attr('data-max_range'));
                    var min_current = Number($(this).attr('data-min-current'));

                    $(this).slider({
                        range: "min",
                        value: min_current,
                        min: min_range,
                        max: max_range
                    });
                });


                need_url = true;

            }
        });

        if(need_url != false){
            var stateParameters = { foo: "bar" };
            history.pushState(stateParameters, "New page title", window.location.href + "/" + flat);
        }

    }

